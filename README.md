![Step Function](step-function.png)

# Variables para la IaC
Crear un archivo con el nombre "variables.tf" y settear los valores de las variables:

```js
# Bucket Name
variable "s3_name" {
    type = string
    default = ""  
}

# ARN Lambda
variable "lambda_custom" {
    type = string
    default = ""
}

# ARN Lambda
variable "lambda_product" {
    type = string
    default = ""
}

# TABLE NAME
variable "table_name" {
    type = string
    default = ""
}

# TABLES ARN
variable "table_arn" {
    type = list(string)
    default = [""]
}

```

# S3 Object
Crear un bucket en s3 y dentro crear otra carpeta con el nombre de "productos-json" y subir el archivo "product-01.json"

# DynamoDB
Al crear la tabla de dynamo asegurarse que solo debe de tener la "clave de partición" = "id" y no "Clave de ordenación"

# Lambda ar-ccom-pim-customize-product-seller-exporter
Cree un lambda y ponga el siguiente código:
```js
exports.handler = async (event) => {
    // TODO implement
    
    // Failed excecute
    // const valor = event.productI.id
    
    // Custom Error
    // if(event.productId) {
    //     return {
    //         status: false, 
    //         error: { 
    //             typeError: "PRODUCT_NO_FOUND",
    //             message: "Nuevo error personalizado"
    //         }
    //     }
    // }
    
    //Execute success
    return {
        status: true,
        data: {
            id: 1,
            name: "Producto 1" ,
            ...event
        }
    }
    
};

```


# Lambda ar-ccom-pim-invoke-product-exporter-vtex
Cree un lambda y ponga el siguiente código:
```js
exports.handler = async (event) => {
    // TODO implement
    return {
        status: true
    }
    
};

```

# Invoke step function
Para invocar la step función, utilize lo siguiente:

```json
{
  "key":"productos-json/product-01.json",
  "id": "1001"
}
```

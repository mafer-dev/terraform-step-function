# Bucket Name
variable "s3_name" {
  type    = string
  default = "testimagesfer"
}

variable "lambda_custom" {
  type    = string
  default = "arn:aws:lambda:us-east-1:332631661714:function:sfn_custom_test"
}

variable "lambda_product" {
  type    = string
  default = "arn:aws:lambda:us-east-1:332631661714:function:sfn_test1"
}

variable "table_name" {
  type    = string
  default = "ar-product-data-exporter"
}

variable "table_name_error" {
  type    = string
  default = "ar-product-data-exporter"
}

variable "table_arn" {
  type = list(string)
  default = [
    "arn:aws:dynamodb:us-east-1:332631661714:table/ar-product-data-exporter",
  ]
}

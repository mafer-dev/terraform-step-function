## Locals variables Argentina
## suffix _ar = infra Argentina
locals {
  country = "cl"
  common_tags = {
    "Terraform" = "yes"
  }
}
locals {
  # IMPORTANT!!!!!!!!: change the name of this variables when it is put into production  

  # Step function  
  sfn_name_product_exporter                 = "${local.country}-ccom-pim-sfn-product-exporter"
  sfn_iam_role_name_product_exporter        = "ccom_pim_sfn_product_exporter_${local.country}"
  sfn_clowdwatch_logs_name_product_exporter = "${local.country}-ccom-pim-sfn-product-exporter-cw-logs"

  #Tables for country
  table_name_product = var.table_name
  dynamodb_tables    = var.table_arn
}

# 1: Rol
resource "aws_iam_role" "role_sfn_product_exporter_ar" {
  name = local.sfn_iam_role_name_product_exporter
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = ["states.amazonaws.com"]
        }
      },
    ]
  })

  tags = merge(
    local.common_tags,
  )
}

# 2: Creación de cloudwatch
resource "aws_cloudwatch_log_group" "cw_log_group_sfn_product_exporter" {
  name              = "/aws/stepfunction/${local.sfn_name_product_exporter}"
  retention_in_days = 30

  tags = merge(
    local.common_tags,
  )
}


resource "aws_iam_policy" "policy_sfn_product_exporter" {
  name = local.sfn_clowdwatch_logs_name_product_exporter
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "logs:CreateLogDelivery",
          "logs:GetLogDelivery",
          "logs:UpdateLogDelivery",
          "logs:ListLogDeliveries",
          "logs:PutLogEvents",
          "logs:PutResourcePolicy",
          "logs:DescribeResourcePolicies",
          "logs:DescribeLogGroups"
        ]
        Effect   = "Allow"
        Resource = "*"
      }
    ]
  })
}

# 3. IAM ATTACHMENT POLICY Step function y Cloudwatch
resource "aws_iam_role_policy_attachment" "role_policy_attach_cw_product" {
  role       = aws_iam_role.role_sfn_product_exporter_ar.name
  policy_arn = aws_iam_policy.policy_sfn_product_exporter.arn
}

# 4. Policy para que el step function tenga permiso de invocar lambdas
resource "aws_iam_policy" "lambda_policy_product_exporter" {
  name = "${local.sfn_iam_role_name_product_exporter}-lambda-policy"
  policy = jsonencode({
    Version : "2012-10-17"
    Statement : [
      {
        Action : ["lambda:InvokeFunction"]
        Effect : "Allow"
        Resource : [
          var.lambda_custom,
          var.lambda_product
        ]
      }
    ]
  })
}

# 5. IAM ATTACHMENT POLICY Step function y lambdas a invocar
resource "aws_iam_role_policy_attachment" "role_policy_attach_lambda_product" {
  role       = aws_iam_role.role_sfn_product_exporter_ar.name
  policy_arn = aws_iam_policy.lambda_policy_product_exporter.arn
}


# 6. Policy for the step function to execute actions on CENCOPIM's S3 bucket
resource "aws_iam_policy" "s3_sfn_policy_product_exporter" {
  name   = "${local.sfn_iam_role_name_product_exporter}-s3-policy"
  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "Stmt1619630578818",
      "Action": [
        "s3:GetObject",
        "s3:DeleteObject",
        "s3:DeleteObjectTagging"
      ],
      "Effect": "Allow",
      "Resource": "arn:aws:s3:::${var.s3_name}/*"
    }
  ]
}
POLICY
}

# 7. IAM ATTACHMENT POLICY Step function y S3
resource "aws_iam_role_policy_attachment" "role_policy_attach_s3_product" {
  role       = aws_iam_role.role_sfn_product_exporter_ar.name
  policy_arn = aws_iam_policy.s3_sfn_policy_product_exporter.arn
}

resource "aws_iam_policy" "dynamodb_policy_exporter" {
  name = "dynamodb_policy_exporter_ar"
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "dynamodb:BatchGetItem",
          "dynamodb:GetItem",
          "dynamodb:Query",
          "dynamodb:Scan",
          "dynamodb:BatchWriteItem",
          "dynamodb:PutItem",
          "dynamodb:UpdateItem",
          "dynamodb:DeleteItem"
        ]
        Effect   = "Allow"
        Resource = local.dynamodb_tables
      },
    ]
  })
}

resource "aws_iam_role_policy_attachment" "role_policy_attach_dynamodb_exporter" {
  role       = aws_iam_role.role_sfn_product_exporter_ar.name
  policy_arn = aws_iam_policy.dynamodb_policy_exporter.arn
}

# 8. Step Functions Cancel
resource "aws_sfn_state_machine" "sfn_product_expoter" {
  name     = local.sfn_name_product_exporter
  type     = "STANDARD"
  role_arn = aws_iam_role.role_sfn_product_exporter_ar.arn
  definition = jsonencode({
    "Comment" : "This SFN allows the processing of products",
    "StartAt" : "Is it new processes or reprocess?",
    "States" : {
      "Is it new processes or reprocess?" : {
        "Type" : "Choice",
        "Choices" : [
          {
            "And" : [
              {
                "Variable" : "$.is_retry",
                "IsPresent" : true
              },
              {
                "Variable" : "$.is_retry",
                "BooleanEquals" : true,
              }
            ],
            "Next" : "Get item from ar-product-data-exporter"
          }
        ]
        "Default" : "GetObject from S3"
      },
      "GetObject from S3" : {
        "Type" : "Task",
        "Parameters" : {
          "Bucket" : var.s3_name,
          "Key.$" : "$.key"
        },
        "Resource" : "arn:aws:states:::aws-sdk:s3:getObject",
        "Next" : "ar-ccom-pim-customize-product-seller-exporter",
        "ResultSelector" : {
          "Body.$" : "$.Body"
        },
        "ResultPath" : "$.S3Obj",
        "Retry" : [{
          "ErrorEquals" : ["S3.SdkClientException", "S3.S3Exception"],
          "IntervalSeconds" : 10,
          "MaxAttempts" : 3,
          "BackoffRate" : 1.5
        }],
        "Catch" : [
          {
            "ErrorEquals" : [
              "States.ALL"
            ],
            "ResultPath" : "$.Error",
            "Next" : "Format ERROR_STATE_MACHINE"
          }
        ],
        "Comment" : "Allows to obtain the file of the product to be processed from the bucket"
      },
      "ar-ccom-pim-customize-product-seller-exporter" : {
        "Type" : "Task",
        "Resource" : "arn:aws:states:::lambda:invoke",
        "Parameters" : {
          "Payload.$" : "$.S3Obj.Body",
          "FunctionName" : var.lambda_custom
        },
        "Catch" : [
          {
            "ErrorEquals" : [
              "States.ALL"
            ],
            "ResultPath" : "$.Error",
            "Next" : "Format ERROR_STATE_MACHINE"
          }
        ],
        "ResultSelector" : {
          "Payload.$" : "$.Payload"
        },
        "ResultPath" : "$.Response",
        "Next" : "Validate_response ar-ccom-pim-customize-product-seller-exporter"
      },
      "Validate_response ar-ccom-pim-customize-product-seller-exporter" : {
        "Type" : "Choice",
        "Choices" : [
          {
            "And" : [
              {
                "Variable" : "$.Response.Payload.status",
                "IsPresent" : true
              },
              {
                "Variable" : "$.Response.Payload.status",
                "BooleanEquals" : true,
              }
            ],
            "Next" : "ar-ccom-pim-invoke-product-exporter-vtex"
          },
          {
            "And" : [
              {
                "Variable" : "$.Response.Payload.status",
                "IsPresent" : true
              },
              {
                "Variable" : "$.Response.Payload.status",
                "BooleanEquals" : false,
              },
              {
                "Variable" : "$.Response.Payload.error",
                "IsPresent" : true
              }
            ],
            "Next" : "Save Error ar-product-data-exporter"
          },
        ]
      },
      "ar-ccom-pim-invoke-product-exporter-vtex" : {
        "Type" : "Task",
        "Resource" : "arn:aws:states:::lambda:invoke",
        "Parameters" : {
          "Payload.$" : "$.Response.Payload",
          "FunctionName" : var.lambda_product
        },
        "Retry" : [{
          "ErrorEquals" : ["Lambda.TooManyRequestsException", "Lambda.ServiceException"],
          "IntervalSeconds" : 10,
          "MaxAttempts" : 5,
          "BackoffRate" : 1.5
        }],
        "Catch" : [
          {
            "ErrorEquals" : [
              "States.ALL"
            ],
            "ResultPath" : "$.Error",
            "Next" : "Format ERROR_STATE_MACHINE"
          }
        ],
        "ResultSelector" : {
          "Payload.$" : "$.Payload"
        },
        "ResultPath" : "$.Response",
        "Next" : "Validate_response ar-ccom-pim-invoke-product-exporter-vtex",
        "Comment" : "Lambda to register or update the product in VTEX"
      },
      "Validate_response ar-ccom-pim-invoke-product-exporter-vtex" : {
        "Type" : "Choice",
        "Choices" : [
          {
            "And" : [
              {
                "Variable" : "$.Response.Payload.status",
                "IsPresent" : true
              },
              {
                "Variable" : "$.Response.Payload.status",
                "BooleanEquals" : true,
              }
            ],
            "Next" : "Delete item from ar-product-data-exporter"
          },
          {
            "And" : [
              {
                "Variable" : "$.Response.Payload.status",
                "IsPresent" : true
              },
              {
                "Variable" : "$.Response.Payload.status",
                "BooleanEquals" : false,
              },
              {
                "Variable" : "$.Response.Payload.error",
                "IsPresent" : true
              }
            ],
            "Next" : "Save Error ar-product-data-exporter"
          },
        ]
      },
      "Delete item from ar-product-data-exporter" : {
        "Type" : "Task",
        "Resource" : "arn:aws:states:::dynamodb:deleteItem",
        "Parameters" : {
          "TableName" : local.table_name_product,
          "Key" : {
            "id.$" : "$.id"
          }
        },
        "ResultPath" : null,
        "Comment" : "Deleting the record from the table",
        "End" : true
      },
      "Save Error ar-product-data-exporter" : {
        "Type" : "Task",
        "Resource" : "arn:aws:states:::dynamodb:updateItem",
        "Parameters" : {
          "TableName" : local.table_name_product,
          "Key" : {
            "id.$" : "$.id"
          },
          "UpdateExpression" : "SET #x1 = :x1, #x2 = :x2, #x3 = :x3, #x4 = :x4, #x5 = :x5",
          "ExpressionAttributeNames" : {
            "#x1" : "data",
            "#x2" : "failure_type",
            "#x3" : "failure_reason",
            "#x4" : "update_at",
            "#x5" : "state_machine_execution_id"
          }
          "ExpressionAttributeValues" : {
            ":x1.$" : "States.JsonToString($.Response.Payload.data)",
            ":x2.$" : "$.Response.Payload.error.type",
            ":x3.$" : "$.Response.Payload.error.message",
            ":x4.$" : "$$.Execution.StartTime",
            ":x5.$" : "$$.Execution.Id"
          }
        },
        "ResultPath" : null,
        "Comment" : "Save error to table",
        "End" : true
      },
      "Get item from ar-product-data-exporter" : {
        "Type" : "Task",
        "Resource" : "arn:aws:states:::dynamodb:getItem",
        "Parameters" : {
          "TableName" : local.table_name_product,
          "Key" : {
            "id.$" : "$.id"
          }
        },
        "ResultSelector" : {
          "Item.$" : "$.Item"
        },
        "ResultPath" : "$.DynamoDB",
        "Catch" : [
          {
            "ErrorEquals" : [
              "States.ALL"
            ],
            "ResultPath" : "$.Error",
            "Next" : "Format ERROR_STATE_MACHINE"
          }
        ],
        "Comment" : "Get the record from the table",
        "Next" : "GetObject from S3"
      },
      "Format ERROR_STATE_MACHINE" : {
        "Type" : "Pass",
        "Parameters" : {
          "data" : {},
          "error" : {
            "type" : "ERROR_STATE_MACHINE",
            "message.$" : "States.JsonToString($.Error)"
          }
        },
        "ResultPath" : "$.Response.Payload",
        "Next" : "Save Error ar-product-data-exporter"
      },
    }
  })

  logging_configuration {
    log_destination        = "${aws_cloudwatch_log_group.cw_log_group_sfn_product_exporter.arn}:*"
    include_execution_data = true
    level                  = "ALL"
  }

  depends_on = [
    aws_iam_role_policy_attachment.role_policy_attach_cw_product,
    aws_iam_role_policy_attachment.role_policy_attach_lambda_product,
    aws_iam_role_policy_attachment.role_policy_attach_s3_product,
    aws_iam_role_policy_attachment.role_policy_attach_dynamodb_exporter
  ]

  tags = merge(
    local.common_tags,
  )
}
